package WheelyMonitor::Example;
use Mojo::Base 'Mojolicious::Controller';
use lib qw(/tk/lib /tk/mojo/lib);
use Util;
my $DB = Util->db(do 'conf/mysql.conf');
use Date::Parse;

sub welcome {
  my $self = shift;
  my $a = $DB->select('select * from cars_log'); 
	my $b;
	for (@$a) {
		push @{$b->{$_->{'car_id'}}}, $_;
	}
  $self->render(
    list => $b,
    );
}

	#for my $i (0..scalar @$a) {
	#	$it = Util::iso2time($a[i]->{"created"}) - Util::iso2time($a[i++]->{"created"});
1;


# id | car_id                   | status | created             |
#+----+--------------------------+--------+---------------------+
#|  4 | 52a83560844a38f65200004a |      1 | 2014-02-24 16:11:16 |
#| 23 | 52a83560844a38f65200004a |      0 | 2014-02-24 16:19:51


#52a83560844a38f65200004a => [ 
#	{ id=> 4, car_id => '52a83560844a38f65200004a', status => 1, created '2014-02-24 16:11:16'},
#	{ id=> 23, car_id => '52a83560844a38f65200004a', status => 0, created '2014-02-24 16:19:51'},
#]