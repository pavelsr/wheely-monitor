package WheelyMonitor;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;
  my $r = $self->routes;
  $r->get('/')->to('example#welcome');
}

1;
